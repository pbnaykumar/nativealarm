/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import MyHeadlessTask from './src/Components/MyHeadlessTask'

import {store} from './store'
import { Provider } from 'react-redux';

AppRegistry.registerHeadlessTask('Heartbeat', () => MyHeadlessTask);
const RNRedux = () => (
    <Provider store={store}>
      <App />
    </Provider>
  );
AppRegistry.registerComponent(appName, () => RNRedux);
