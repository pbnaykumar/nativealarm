/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Alarm from './src/views/Alarm'
import Home from './src/views/Home'

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import GetLocation from './src/views/GetLocation';
// import { createStackNavigator } from 'react-navigation';


const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  const linking = {
    prefixes: ['mychat://', 'https://mychat.com'],
  };
  const Stack = createNativeStackNavigator(
    // Home,
    // Alarm,
    // GetLocation
  );
  const config = {
    screens: {
      Home: "a",
      Alarm:"b",
      GetLocation:"c"
      
    }
  }
  return (
    <NavigationContainer linking={{
      prefixes: ["mychat://app"],
      config
    }}>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Alarm" component={Alarm} />
        <Stack.Screen name="GetLocation" component={GetLocation} />
      </Stack.Navigator>
      </NavigationContainer>

  );

//   const Stack = createStackNavigator({
   
//     Alarm,
//     Home,
// });

// return <Stack />
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
