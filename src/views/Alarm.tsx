import React from 'react';
import {
  StyleSheet, Text, View, TouchableOpacity, Image,
} from 'react-native';
import { connect } from 'react-redux';
import Heartbeat from '../../Heartbeat';
import heart from '../assets/heart.png';



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  view: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'gray',
    padding: 10,
    margin: 10,
  },
  text: {
    fontSize: 20,
    color: 'white',
  },
});


const Work = (props) => {
  const heartBeat=props.heartBeat
  console.log(heartBeat)
  if(heartBeat===true){
    console.log('vitare')
    // props.navigation.navigate('GetLocation')
    // startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(
    //   "market://details?id=com.mycompany.mypackage")));
  }
//   const imageSize = heartBeat ? 150 : 100;
  const imageSize =  100;
  // VibratePhone()
  return (
    <View style={styles.container}>
      <View style={styles.view}>
        <Image source={heart} style={{ width: imageSize, height: imageSize }} resizeMode="contain" />
      </View>
      <View style={styles.view}>
        <TouchableOpacity style={styles.button} onPress={() =>{ Heartbeat.startService()}}>
          <Text >Start</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => Heartbeat.stopService()}>
          <Text style={styles.instructions}>Stop</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = store => ({
  heartBeat: store.App.heartBeat,
});

export default connect(mapStateToProps)(Work);
// export default Work;
